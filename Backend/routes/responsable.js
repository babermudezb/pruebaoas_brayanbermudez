// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de responsable
var responsable_controller = require('../controllers/responsable');

// GET /:id
router.get('/:id', responsable_controller.responsable_details);
// GET /
router.get('/', responsable_controller.responsable_all);
// POST /
router.post('/', responsable_controller.responsable_create);
// PUT /:id
router.put('/:id', responsable_controller.responsable_update);
// DELETE /:id
router.delete('/:id', responsable_controller.responsable_delete);

//export router
module.exports = router;