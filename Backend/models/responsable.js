//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Responsable
var ResponsableSchema = new Schema({
nombres: {type:String, required:true}, 
apellidos: {type:String, required:true}, 
email: {type:String, required:true}, 
telefono: {type:Number, required:true}, 

});

ResponsableSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Responsable', ResponsableSchema);