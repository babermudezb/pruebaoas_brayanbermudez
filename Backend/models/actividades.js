//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Actividades
var ActividadesSchema = new Schema({
fecha_creacion: {type:String, required:true}, 
fecha_limite: {type:String, required:true}, 
responsable:[ {type:Schema.Types.ObjectId, ref: 'Responsable' , autopopulate: true }], 
descripcion: {type:String, required:true}, 
estado: {type:String, required:true}, 

});

ActividadesSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Actividades', ActividadesSchema);