//importar modelo
var Actividades = require('../models/actividades');

//funcion create
exports.actividades_create = function (req, res) {
    var actividades = new Actividades(
        req.body
    );

    actividades.save(function (err, actividades) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: actividades._id})
    })
};
//funcion read by id
exports.actividades_details = function (req, res) {
    Actividades.findById(req.params.id, function (err, actividades) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(actividades);
    })
};
//funcion read all
exports.actividades_all = function (req, res) {
    Actividades.find(req.params.id, function (err, actividades) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(actividades);
    })
};
//funcion update
exports.actividades_update = function (req, res) {
    Actividades.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, actividades) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", actividades: actividades });
    });
};
//funcion add responsable
exports.Actividades_add_responsable = function (req, res) {
Actividades.findByIdAndUpdate(req.params.id, { $push: { responsable: req.body.responsable } }, { new: true }, function (err, Actividades) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Actividades: Actividades });
});
};

//funcion delete
exports.actividades_delete = function (req, res) {
    Actividades.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};