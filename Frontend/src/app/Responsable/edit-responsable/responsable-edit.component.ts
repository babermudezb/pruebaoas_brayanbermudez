import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { ResponsableService } from '../../services/responsable.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-responsable-edit',
  templateUrl: './responsable-edit.component.html',
  styleUrls: ['./responsable-edit.component.css']
})
export class ResponsableEditComponent implements OnInit {
  
  angForm: FormGroup;
  responsable: any = {};

  constructor(private route: ActivatedRoute,
    
    private router: Router,
    public toastr: ToastrManager,
    private bs: ResponsableService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombres: ['', Validators.required ],
apellidos: ['', Validators.required ],
email: ['', Validators.required ],
telefono: ['', Validators.required ]

      });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editResponsable(params['id']).subscribe(res => {
        this.responsable = res;
      });
    });
     
  }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateResponsable(nombres, apellidos, email, telefono ) {
   this.route.params.subscribe(params => {
      this.bs.updateResponsable(nombres, apellidos, email, telefono  ,params['id']);
      this.showInfo()
      this.router.navigate(['responsable']);
   });
}
}
