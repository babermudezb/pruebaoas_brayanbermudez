import { Component, OnInit } from '@angular/core';
import Responsable from '../../models/Responsable';
import { ResponsableService } from '../../services/responsable.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-responsable-get',
  templateUrl: './responsable-get.component.html',
  styleUrls: ['./responsable-get.component.css']
})

export class ResponsableGetComponent implements OnInit {

  responsable: Responsable[];

  constructor( private bs: ResponsableService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getResponsable()
      .subscribe((data: Responsable[]) => {
        this.responsable = data;
    });
  }

  deleteResponsable(id) {
    this.bs.deleteResponsable(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getResponsable()
      .subscribe((data: Responsable[]) => {
        this.responsable = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

