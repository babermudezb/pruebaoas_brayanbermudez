import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResponsableService {

  uri = 'http://localhost:3025/responsable';

  constructor(private http: HttpClient) { }

  addResponsable(nombres ,apellidos ,email ,telefono ) {
    const obj = {
      nombres:nombres,
apellidos:apellidos,
email:email,
telefono:telefono

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }

  getResponsable() {
    return this
           .http
           .get(`${this.uri}`);
  }

  editResponsable(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }

  updateResponsable(nombres ,apellidos ,email ,telefono , id) {

    const obj = {
      nombres:nombres,
apellidos:apellidos,
email:email,
telefono:telefono

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }

 deleteResponsable(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
