import { Component, OnInit } from '@angular/core';
import Actividades from '../../models/Actividades';
import { ActividadesService } from '../../services/actividades.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-actividades-get',
  templateUrl: './actividades-get.component.html',
  styleUrls: ['./actividades-get.component.css']
})

export class ActividadesGetComponent implements OnInit {

  actividades: Actividades[];

  constructor( private bs: ActividadesService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getActividades()
      .subscribe((data: Actividades[]) => {
        this.actividades = data;
    });
  }

  deleteActividades(id) {
    this.bs.deleteActividades(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getActividades()
      .subscribe((data: Actividades[]) => {
        this.actividades = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

