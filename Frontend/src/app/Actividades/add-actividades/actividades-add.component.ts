import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Responsable from '../../models/Responsable';
import { ActividadesService } from '../../services/actividades.service';
import { ResponsableService } from '../../services/responsable.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-actividades-add',
  templateUrl: './actividades-add.component.html',
  styleUrls: ['./actividades-add.component.css']
})
export class ActividadesAddComponent implements OnInit {
   Responsable: Responsable[];
  angForm: FormGroup;
  constructor(private ResponsableSer: ResponsableService,private fb: FormBuilder, private Actividades_ser: ActividadesService, public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       fecha_creacion: ['', Validators.required ],
fecha_limite: ['', Validators.required ],
responsable: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addActividades(fecha_creacion, fecha_limite, responsable, descripcion, estado ) {
    this.Actividades_ser.addActividades(fecha_creacion, fecha_limite, responsable, descripcion, estado );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.ResponsableSer
.getResponsable()
.subscribe((data: Responsable[]) => {
this.Responsable = data;
});

  }

}
