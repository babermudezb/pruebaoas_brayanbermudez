import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Responsable from '../../models/Responsable';
import { ActividadesService } from '../../services/actividades.service';
import { ResponsableService } from '../../services/responsable.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-actividades-edit',
  templateUrl: './actividades-edit.component.html',
  styleUrls: ['./actividades-edit.component.css']
})
export class ActividadesEditComponent implements OnInit {
  Responsable: Responsable[];
  angForm: FormGroup;
  actividades: any = {};

  constructor(private route: ActivatedRoute,
    private ResponsableSer: ResponsableService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: ActividadesService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        fecha_creacion: ['', Validators.required ],
fecha_limite: ['', Validators.required ],
responsable: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ]

      });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editActividades(params['id']).subscribe(res => {
        this.actividades = res;
      });
    });
     this.ResponsableSer
.getResponsable()
.subscribe((data: Responsable[]) => {
this.Responsable = data;
});

  }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateActividades(fecha_creacion, fecha_limite, responsable, descripcion, estado ) {
   this.route.params.subscribe(params => {
      this.bs.updateActividades(fecha_creacion, fecha_limite, responsable, descripcion, estado  ,params['id']);
      this.showInfo()
      this.router.navigate(['actividades']);
   });
}
}
