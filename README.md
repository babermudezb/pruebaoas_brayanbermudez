# PRUEBA TÉCNICA OAS

Aplicación pedida como prueba técnica para cargo de desarrollador en la OAS de la Universidad Distrital Francisco José de Caldas.  
_el aplicativo desarrollado utiliza docker-compose.yml para compilar los contenedores y correrlos en una instancia local_





### Pre-requisitos 

Docker 
_docker-compose_



### Instalación de Aplicación

_Clonar repositorio_

```
git clone https://gitlab.com/babermudezb/pruebaoas_brayanbermudez
```

_entrar carpeta_

```
cd pruebaoas_brayanbermudez 
```
_generar contenedores_

```
docker-compose up
```
## NOTA 

_De presentarse un error (CONNECTION REFUSED) en el backend es necesario reiniciar el contenedor del mismo_


## Autor



* **Brayan Bermúdez** - *Prueba Técnica* - [babermudezb](https://gitlab.com/babermudezb/)

